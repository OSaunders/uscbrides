<?php
    require "view/header.php"
?>

<body>
<main>
    <div class="container signup-container">
        <div class ="row">
            <div class="signup-form">
                <h3>Please enter payment information below. Each ride will be $2!</h3>
                <br>
                <div class="form-group">
                     <input type="name" name="name" class="form-control" placeholder="Name on Card...">
                </div>
                <div class="form-group">
                     <input type="card_number" name="crd_num" class="form-control" placeholder="Credit Card Number...">
                </div>
                <div class="form-group">
                     <input type="cvc_number" name="cvc_num" class="form-control" placeholder="CVC Number...">
                </div>
                <div class="form-group">
                     <input type="mm_yy" name="mm_yy" class="form-control" placeholder="MM/YY...">
                </div>
                <div class="form-group">
                    <input type="submit" class="btnSubmit" name="signup_button" value="Submit">
                    <a style="float: right" href ="">back</a>
                </div>
            </div>
        </div>
    </div>    
</main>
</body>
<?php
    require "view/footer.php";
?>

