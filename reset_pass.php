

<?php
    require "view/header.php";
?>
   
<main>
    <div class="container signup-container">
        <div class ="row">
            <div class="signup-form">
                <h2>Password Reset</h2>
                <form>
                    <div><p>Type your new password</p></div>
                    <div class="form-group">
                        <input type="password" name="pwd" class="form-control" placeholder="Password...">
                    </div>
                    <div><p>Retype your new password</p></div>
                    <div class="form-group">
                        <input type="password" name="pwd2" class="form-control" placeholder="Retype Password...">
                    </div>
                    <div>
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" name="reset_button" value="Submit">
                    </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<?php
require "view/footer.php";
?>
