<?php
    require "model/database.php";
    $error_array = array();
    require "includes/form_handlers/login_handler.php";
?>
<!DOCTYPE html>
<html>

<!-- the head section -->
<head>
    <meta charset="utf-8">
    <meta name="description" content="This is an example of a meta description. Will only show up in search results.">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>USCB RIDES</title>
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="styles/main.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    
</head>

<body>

 <header class="container-header">
  
</header>

<main>
   
    <div class="container login-container">
        <div class ="row">
            <div class="driver-login-form">
                <h2>Driver</h2>
                <form action="login_page.php" method="POST">
                    <div class="form-group" action="" method="POST">
                        
                        <input type ="email" class ="form-control" name="dr_log_email" placeholder="USCB Email..." value ="<?php 
                        if(isset($_SESSION['dr_log_email']))
                        {
                            echo $_SESSION['dr_log_email'];
                            }?>" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="dr_pwd" placeholder="Password...">
                    </div>
                    
                    <div class="form-group">
                        <input type ="submit" class="btnSubmit" name="dr_login_button" value="Login">
                        <br>
                        <?php if(in_array("Email or password was incorrect<br>", $error_array)) echo "Email or password was incorrect<br>"; ?>
                    </div>
                    
                    <div class="form-group">
                        <a href="./forgot_pass.php" class="ForgetPwd">Forgot Password?</a>
                    </div>
                    <div class="form-group">
                        <a href="drivers/driver_signup_page.php" class ="Register">Sign Up to Drive</a>
                    </div>
                </form>
            </div>
            <div class="rider rider-login-form">
                <h2>Rider</h2>
                <form action="login_page.php" method="POST">
                    <div class="form-group">
                        <input type ="email" class ="form-control" name="rr_log_email" placeholder="USCB Email..."value="<?php
                        if(isset($_SESSION['rr_log_email']))
                        {
                            echo $_SESSION['rr_log_email'];
                            }?>"required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="rr_pwd" placeholder="Password...">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" name="rr_login_button" value="Login"/>
                        <br>
                        <?php if(in_array("Email or password was incorrect<br>", $error_array)) echo "Email or password was incorrect<br>"?>
                    </div>
                    <div class="form-group">
                        <a href="./forgot_pass.php" class="ForgetPwd">Forgot Password?</a>
                    </div>
                    <div class="form-group">
                        <a href="riders/rider_signup_page.php" class ="Register">Sign Up to Ride</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<?php
require "view/footer.php";
?>
