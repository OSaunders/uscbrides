

<?php
    require "view/header.php";
?>
  
<main>
    <div class="container signup-container">
        <div class ="row">
            <div class="signup-form">
                <h2>Account Recovery</h2>
                <form>
                    <div><p>Enter your account email below and a recovery link will be sent.</p></div>
                    <div class="form-group" action="model/signup.driver.php" method="post">
                        <input type =" text" name="email" class ="form-control" placeholder="USCB Email...">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" name="signup_button" value="Submit">
                    </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<?php
require "view/footer.php";
?>
