<?php
require "../model/database.php";
require "../includes/form_handlers/driver_signup_handler.php";

?>
<!DOCTYPE html>
<html>

<!-- the head section -->
<head>
    <meta charset="utf-8">
    <meta name="description" content="This is an example of a meta description. Will only show up in search results.">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>USCB RIDES</title>
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="../styles/main.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    
</head>

<body>

 <header class="container-header">
  
</header>
<main>
    <div class="container signup-container">
        <div class ="row">
            <div class="signup-form">
                  <br>
                  <!-- Email Errors -->
                        <?php if(in_array("Email already in use<br>", $error_array)) echo "Email already in use<br>";
                        else if(in_array("Invalid format<br>", $error_array)) echo "Invalid format<br>";
                        else if(in_array("Not a valid USCB email<br>", $error_array)) echo "Not a valid USCB email<br>";
                        else if (in_array("Emails do not match<br>", $error_array)) echo "Emails do not match<br>"?>
                  <!-- Password Errors -->
                        <?php if(in_array("Your passwords do not match<br>", $error_array)) echo "Your passwords do not match<br>";
                        else if(in_array("Your passwords can only contain letters and numbers<br>", $error_array)) echo "Your passwords can only contain letters and numbers<br>";
                        else if(in_array("Your passwords must contain atleast 8 characters<br>", $error_array)) echo "Your passwords must contain atleast 8 characters<br>";?>
                  <!-- Name Errors-->
						<?php if(in_array("Your first name must be between 2 and 25 characters<br>", $error_array)) echo "Your first name must be between 2 and 25 characters<br>"; ?>
						<?php if(in_array("Your last name must be between 2 and 25 characters<br>", $error_array)) echo "Your last name must be between 2 and 25 characters<br>"; ?>
						<?php if(in_array("Phone length invalid<br>", $error_array)) echo "Phone length invalid<br>"; ?>
                  <!-- Car Errors-->
						<?php if(in_array("Car make length too short<br>", $error_array)) echo "Car make length too short<br>"; ?>
						<?php if(in_array("Car model length too short<br>", $error_array)) echo "Car model length too short<br>"; ?>
						<?php if(in_array("Car color length too short<br>", $error_array)) echo "Car color length too short<br>"; ?>
						<?php if(in_array("License plate length too short<br>", $error_array)) echo "License plate length too short<br>"; ?>
						<?php if(in_array("Invalid year<br>", $error_array))echo "Invalid year<br>"; ?>
                  
                <h3>Account Information</h3>
                <form action="driver_signup_page.php" method="POST">
                    <div class="form-group" action="model/signup.driver.php" method="post">
                        <input type =" text" name="reg_email" class ="form-control" placeholder="USCB Email..." value="<?php
                        if(isset($_SESSION['reg_email']))
                        {
                            echo $_SESSION['reg_email'];
                        }?>" >
                    </div>
                    <div class="form-group">
                        <input type =" text" name="reg_email2" class ="form-control" placeholder="Retype USCB Email..."value="<?php
                        if(isset($_SESSION['reg_email2']))
                        {
                            echo $_SESSION['reg_email2'];
                        }?>">
                        
                    </div>
                    <div class="form-group">
                        <input type="password" name="reg_pwd" class="form-control" placeholder="Password...">
                    </div>
                    <div class="form-group">
                        <input type="password" name="reg_pwd2" class="form-control" placeholder="Retype Password...">
                    </div>
                    <div>
                        <h3>Personal Information</h3>
                    </div>
                    <div class="form-group">
                        <input type="text" name="reg_fname" class="form-control" placeholder="First Name..."value="<?php
                        if(isset($_SESSION['reg_fname']))
                        {
                            echo $_SESSION['reg_fname'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="reg_lname" class="form-control" placeholder="Last Name..."value="<?php
                        if(isset($_SESSION['reg_lname']))
                        {
                            echo $_SESSION['reg_lname'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="reg_number" class="form-control" placeholder="Phone Number..."value="<?php
                        if(isset($_SESSION['reg_number']))
                        {
                            echo $_SESSION['reg_number'];
                        }?>">
                    </div>
                    <div>
                        <h3>Vehicle Information</h3>
                    </div>
                    <div class="form-group">
                        <input type="text" name="car_make" class="form-control" placeholder="Car Make..."value="<?php
                        if(isset($_SESSION['car_make']))
                        {
                            echo $_SESSION['car_make'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="car_model" class="form-control" placeholder="Car Model..."value="<?php
                        if(isset($_SESSION['car_model']))
                        {
                            echo $_SESSION['car_model'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="car_color" class="form-control" placeholder="Car Color..."value="<?php
                        if(isset($_SESSION['car_color']))
                        {
                            echo $_SESSION['car_color'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="car_year" class="form-control" placeholder="Car year..."value="<?php
                        if(isset($_SESSION['car_year']))
                        {
                            echo $_SESSION['car_year'];
                        }?>">
                    </div>
                    <div class="form-group">
                        <input type="text" name="car_plate_number" class="form-control" placeholder="License Plate Number..."value="<?php
                        if(isset($_SESSION['car_plate_number']))
                        {
                            echo $_SESSION['car_plate_number'];
                        }?>">
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" name="signup_button" value="Register">
                        <a style="float: right" href ="../index.php"> back</a>

                        <br>
                        <?php if(in_array("<span style ='color: #14C800;'> Registration Complete</span><br>", $error_array)) echo "<span style ='color: #14C800;'> Registration Complete</span><br>"; ?>
                    </div>
                </form>
            </div>
        </div>
</div>
</main>

<?php
require "../view/footer.php";
?>

