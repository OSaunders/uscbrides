<?php
    require "../view/drivers_header.php";
    include("../includes/classes/Member.php");
    include("../includes/classes/Post.php");
    require "driver_details.php";
    
    if(isset($_POST['post_button']))
    {
        $post = new Post($con, $userLoggedIn);
        $post->submitPost($_POST['drop_off'], $_POST['date'], $_POST['appt'], $_POST['seats'], 'none');
        
        //$_SESSION['username'] = $username;
            header("Location: ../drivers/driver_sub.php");
            exit();
    }
?>

<body>
<main>
    
    <div class="main_column column"><!-- this was the signup-container-->
        <!--<div class ="row">-->
            <!--<div class="signup-form">-->
                <h3>Welcome to USCB Rides! LETS GET THIS BREAD!!</h3>
                <br>
                <form action="driver_landing_page.php" method="POST">
                    <h6>Please select what campus you're going to:</h6>
                    <div class="form-group">
                        <select class="custom-select" name="drop_off" required>
                        <option value="USCB Bluffton Campus">USCB Bluffton Campus</option>
                        <option value="USCB Beaufort Campus">USCB Beaufort Campus</option>
                        <option value="USCB Hilton Head Campus">USCB Hilton Head Campus</option>
                      </select>
                    </div>
    <div class="invalid-feedback">Example invalid destination select feedback</div>
    
                    <label for="start">Please enter the date your departure:</label>
                    <input type="date" id="start" name="date"
                           value="YYYY-MM-DD"
                           min="2019-01-01" max="2019-12-31">
                    
                    <label for="appt">Please choose a time for meet up:</label>
                    <input type="time" id="appt" name="appt"
                           min="9:00" max="18:00" required>
                   
                    <h6>Please select the number of passengers you are willing to take with you:</h6>
                    <div class="form-group">
                        <select class="custom-select" name="seats" required>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                      </select>
                        
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" name="post_button" value="Submit">
                        <a href ="">back</a>
                    </div>
                    
                </form>
            <!--</div>-->
        <!--</div>-->
    </div>
</div>
</main>
</body>
<?php
require "../view/footer.php";
?>

