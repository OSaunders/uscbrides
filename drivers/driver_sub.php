<?php
    require "../view/drivers_header.php";
    require "driver_details.php";
?>

<body>   
<main>
    <div class="main_column column"><!-- was confirm-container-->
        <!--<div class ="row">-->
            <!--<div class="confirm">-->
                <h2><b>Thanks for Posting!</b></h2>
                <h3>Your ride is now listed. :-)</h3>
                <h4>A confirmation email will be sent to your USC email account.</h4>
                <!-- insert links -->
                <div>
                <a href="driver_landing_page.php"><button type="button">Create Listing</button></a>
                <a href="listings_page.php"><button type="button">Current Listings</button></a>
                <a href="index.php"><button type="button">Return Home</button></a>
                </div>
            <!--</div> -->
        <!--</div>-->
    </div>
    
    
</div>
</main>
</body>
<?php
require "../view/footer.php";
?>
