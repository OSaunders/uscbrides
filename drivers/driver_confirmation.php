<?php
    require "../view/drivers_header.php";
    require "driver_details.php";
?>

<body>   
<main>
    <div class="confirm-container">
        <div class ="row">
            <div class="confirm">
                <h2><b>Thanks for signing up!</b></h2>
                <h3>You are now registered to drive with USCB Rides. :-)</h3>
                <p>A confirmation email was sent to your USC email account!</p>
                <!-- insert links -->
                <div>
                <a href="driver_landing_page.php"><button type="button">Create Listing</button></a>
                <a href="../index.php"><button type="button">Return Home</button></a>
                </div>
            </div>
        </div>
    </div>
    
    
</div>
</main>
</body>
<?php
require "../view/footer.php";
?>

