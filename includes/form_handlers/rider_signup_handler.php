<?php

$fname = "";
$lname = "";
$email = "";
$email2 = "";
$password = "";
$password2 = "";
$phone = "";
$date = "";
$error_array = array();

if (isset($_POST['signup_button'])) {

    //Registration form values
    //First Name 
    $fname = strip_tags($_POST['reg_fname']); //for security removes tags
    $fname = str_replace(' ', '', $fname); //for security removes space
    $fname = ucfirst(strtolower($fname)); //makes the first letter upper case
    $_SESSION['reg_fname'] = $fname; //Stores first name into session variable
    //Lase Name
    $lname = strip_tags($_POST['reg_lname']); //for security removes tags
    $lname = str_replace(' ', '', $lname); //for security removes space
    $lname = ucfirst(strtolower($lname)); //makes the first letter upper case
    $_SESSION['reg_lname'] = $lname; //Stores last name into session variable
    //Email
    $email = strip_tags($_POST['reg_email']); //for security removes tags
    $email = str_replace(' ', '', $email); //for security removes space
    $email = ucfirst(strtolower($email)); //makes the first letter upper case
    $_SESSION['reg_email'] = $email; //Stores email into session variable
    //EMAIL2
    $email2 = strip_tags($_POST['reg_email2']); //for security removes tags
    $email2 = str_replace(' ', '', $email2); //for security removes space
    $email2 = ucfirst(strtolower($email2)); //makes the first letter upper case
    $_SESSION['reg_email2'] = $email2; //Stores email2 into session variable
    //PASSWORD
    $password = strip_tags($_POST['reg_pwd']); //for security removes tags
    $password2 = strip_tags($_POST['reg_pwd2']); //for security removes tags
    //PHONE NUMBER
    $phone = strip_tags($_POST['reg_number']); //for security removes tags
    $phone = str_replace(' ', '', $phone); //for security removes space
    $_SESSION['reg_number'] = $phone; //Stores phone number into session variable

    $date = date("Y-m-d");


    $acceptedDomains = array('email.uscb.edu', 'email.sc.edu');

    // Check if email is in valid format
    if ($email == $email2) {
        // Check if email is in valid format
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);

            $parts = explode('@', $email);

            $domain = array_pop($parts);

            if (!in_array($domain, $acceptedDomains)) {
                array_push($error_array, "Not a valid USCB email<br>");
            }
            //Check if email already exsist
            $emailCheck = mysqli_query($con, "SELECT email FROM members WHERE email='$email'");

            //Counts the number of rows returned
            $numOfRows = mysqli_num_rows($emailCheck);

            if ($numOfRows > 0) {
                array_push($error_array, "Email already in use<br>");
            }
        } else {
            array_push($error_array, "Invalid format<br>");
        }
    } else {
        array_push($error_array, "Emails do not match<br>");
    }

    // Validate name
    if (strlen($fname) > 25 || strlen($fname) < 2) {
        array_push($error_array, "Your first name must be between 2 and 25 characters<br>");
    }

    if (strlen($lname) > 25 || strlen($lname) < 2) {
        array_push($error_array, "Your last name must be between 2 and 25 characters<br>");
    }

    // Validate Phone number
    if (strlen($phone) != 10) {
        array_push($error_array, "Phone length invalid<br>");
    }

    // Password Validation
    if ($password != $password2) {
        array_push($error_array, "Your password do not match<br>");
    } else {
        if (preg_match('/[^A-Za-z0-9]/', $password)) {
            array_push($error_array, "Your password can only contain letters and numbers<br>");
        }
    }

    if (strlen($password) > 30 || strlen($password) < 8) {
        array_push($error_array, "Your password must contain atleast 8 characters<br>");
    }
    // Pushes information into the database

    if (empty($error_array)) {
        $password = md5($password); //Encrypt password before sending it to the database
        //Generates username by concatenating first and last name
        $username = strtolower($fname . "_" . $lname);
        $check_username_query = mysqli_query($con, "SELECT username FROM members WHERE username='$username'");

        $i = 0;
        //if username exits add number to username
        while (mysqli_num_rows($check_username_query) != 0) {
            $i++;
            $username = $rusername . "_" . $i;
            $check_username_query = mysqli_query($con, "SELECT username FROM members WHERE username='$username'");
        }
        
           //Profile Pic assignment
        $rand = rand(1, 2); //Random number between 1 and 2
        if ($rand == 1) {
            $profile_pic = "../images/profile_pics/defaults/head_deep_blue.png";
        } else if ($rand == 2) {
            $profile_pic = "../images/profile_pics/defaults/head_sun_flower.png";
        } else if ($rand == 3){
            $profile_pic = "../images/profile_pics/defaults/head_emerald.png";
        } else if ($rand == 4){
            $profile_pic = "../images/profile_pics/defaults/head_red.png";
        } else if ($rand == 5){
            $profile_pic = "../images/profile_pics/defaults/head_turqoise.png";
        }

        $query = mysqli_query($con, "INSERT INTO members VALUES('', '$fname', '$lname', '$username', '$email', '$password', '$phone', '$date', '$profile_pic', 'no', '0', '0','no')");


        array_push($error_array, "<span style ='color: #14C800;'> Registration Complete</span><br>");




        require '../../PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        $mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'uscbrides@gmail.com';                 // SMTP username
        $mail->Password = '@Kwiatek11';                           // SMTP password

        $mail->SMTPSecure = 'ssl';
        //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //$mail->Port = 587;                                    // TCP port to connect to
        $mail->Port = 465;
        //$mail->Port = 25;                                    // TCP port to connect to

        $mail->setFrom('uscbrides@gmail.com', 'USCBrides');
        $mail->addAddress($_POST['reg_email'], 'user');     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
//$mail->addReplyTo('uscbrides@example.com', 'Information');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Welcome to USCBrides!';
        $mail->Body = 'Thank you for registering at USCBrides. From now on your are officialy our <b>Rider</b>! You may now login using your credentials in order to pick your ride!';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }

        //Clear session varables
        $_SESSION['reg_fname'] = "";
        $_SESSION['reg_lname'] = "";
        $_SESSION['reg_email'] = "";
        $_SESSION['reg_email2'] = "";
        $_SESSION['reg_number'] = "";

        $_SESSION['username'] = $username;
        header("Location: rider_confirmation.php");
        exit();
    }
}
?>