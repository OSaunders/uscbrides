<?php
class Member{
    private $member;
    private $con;
    
    public function __construct($con, $member)
    {
        $this->con = $con;
        $user_details_query = mysqli_query($con, "SELECT * FROM members WHERE username='$member'");
        $this->member = mysqli_fetch_array($user_details_query);
    }
    
    public function getMembername()
    {
        return $this->member['username'];
    }
    
    public function getNumPosts()
    {
        $username = $this->member['username'];
        $query = mysqli_query($this->con, "SELECT num_posts FROM members WHERE username = '$username'");
        $row = mysqli_fetch_array($query);
        return $row['num_posts'];
    }
    
    public function getFirstAndLastName()
    {
        $username = $this->member['username'];
        $query = mysqli_query($this->con, "SELECT firstName, lastName FROM members WHERE username='$username'");
        $row = mysqli_fetch_array($query);
        return $row['firstName']. " ".$row['lastName'];
    }
    
    public function isClosed()
    {
        $username = $this->member['username'];
        $query = mysqli_query($this->con, "SELECT user_closed FROM members WHERE username = '$username'");
        $row = mysqli_fetch_array($query);
        
        if($row['user_closed'] == 'yes')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
?>

