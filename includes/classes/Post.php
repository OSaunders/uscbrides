<?php
class Post {
    private $member_obj;
    private $con;
    
    public function __construct($con, $member)
    {
        $this->con = $con;
        $this->member_obj = new Member($con, $member);
        
    }
    
    
    public function submitPost($drop_off, $date, $appt, $seats, $user_to)
    {
        //Gets the member's username
       $added_by = $this->member_obj->getMembername();
       
       //stores into databse
       $query = mysqli_query($this->con, "INSERT INTO avaliable_ride VALUES('', '$date', '$appt', '$drop_off', '$seats', 'no', 'no', '$added_by', '$user_to')");
       $return_id = mysqli_insert_id($this->con);
       
       //Insert notification
       
       ////////////////////////////////////send email notification/////////////////////////////////////
       
      
       
       
       ////////////////////////////////////////////////////////////////////////////////////////////
       
       //Update post count for member
       $num_posts = $this->member_obj->getNumPosts();
       $num_posts++;
       $update_query = mysqli_query($this->con, "UPDATE members SET num_posts = '$num_posts' WHERE username = '$added_by'");
    }
    
    public function loadPostMembers()
    {
        
        
        $str = ""; //TODO remove if not needed String to return
        $data_query = mysqli_query($this->con, "SELECT * FROM avaliable_ride WHERE deleted = 'no' ORDER BY idAvaliableRide DESC");
        
        if(mysqli_num_rows($data_query) > 0)
        {
            $num_iterations = 0;
            $count = 1;
        
        while($row = mysqli_fetch_array($data_query))
        {
            $id = $row['idAvaliableRide'];
            $date = $row['date'];
            $appt = $row['time'];
            $drop_off = $row['location'];
            $seats = $row['seats'];
            $added_by = $row['added_by'];
            
            //TODO remove if not needed
            //Prepare user_to so it can be included even if not posted to a user
            
            if($row['user_to'] == "none")
            {
                $user_to = "";
            }
            else
            {
                $user_to_obj = new Member($con, $row['user_to']);
                $user_to_name = $user_to_obj->getFirstAndLastName();
                $user_to = "to <a href'" . $row['user_to'] ."'>". $user_to_name . "</a>";
            }
            
            //Check if member who posted, has their account closed
            $added_by_obj = new Member($this->con, $added_by);
            if($added_by_obj->isClosed())
            {
                continue;
            }
            
            $userLoggedIn = $this->member_obj->getMembername();
            $user_logged_obj = new Member($this->con, $userLoggedIn);
           
            
            if($userLoggedIn != $added_by)
                $accept_button = "<button style = 'float:right'class='accept_button btn-success' id='post$id'>Accept</button>";
            else 
                $accept_button="";
                
            
            
            
            
            $user_details_query = mysqli_query($this->con, "SELECT firstName, lastName, profile_pic FROM members WHERE username = '$added_by'");
            $user_row = mysqli_fetch_array($user_details_query);
            $first_name = $user_row['firstName'];
            $last_name = $user_row['lastName'];
            $profile_pic = $user_row['profile_pic'];
           
            $str .="<div class= 'member_post'>
                            <div class='post_profile_pic'>
                            <img src='$profile_pic' width = '50'>
                            </div>
                         
                            <div class ='posted_by' style='color:#ACACAC;'>
                                <a href = '$added_by'> $first_name $last_name </a> $user_to
                                    $accept_button
                            </div>
                            <div id ='post_body'>
                            Departure date: $date
                            <br>
                            Departure time: $appt
                            <br>
                            Destination: $drop_off
                            <br>
                            Avaliable seats: $seats
                                
                            <br>
                            </div>
                         </div> 
                         <hr>";
            
            ?>
            <script>
                $(document).ready(function(){
                    $('#post<?php echo $id; ?>').on('click', function(){
                        bootbox.comfirm("Are you sure you want to deny this ride?", function(result){
                            $.post("includes/form_handlers/delete_post.php?post_id=<?php echo $id; ?>", {result:result});
                            
                            if(result)
                                location.reload();
                        });
                    });
                });
            </script> 
             <?php
        }//end while loop
        
        
        }// end if
        echo $str;
    }
}
?>