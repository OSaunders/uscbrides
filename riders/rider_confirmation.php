<?php
    require "../view/riders_header.php";
    require "rider_details.php"
?>

<body>   
<main>
    <div class="confirm-container">
        <div class ="row">
            <div class="confirm">
                <h2><b>Thanks for signing up!</b></h2>
                <h3>You are now registered with USCB Rides. :-)</h3>
                <p>A confirmation email was sent to your USC email account!</p>
                <div class="button-group">
                    <a href="rider_landing_page.php"><button type="button">Current Listings</button></a>
                <a href="../index.php"><button type="button">Return Home</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
</body>
<footer>
</footer>
<?php
require "../view/footer.php";
?>

