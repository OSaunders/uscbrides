<?php
    require "../view/riders_header.php";
    include("../includes/classes/Member.php");
    include("../includes/classes/Post.php");
    require "rider_details.php"
?>

<body>
    <main>
        <div class="main_column column">
            <form class="post_form" action="listings_page.php" method="POST">
                <h3>Select your ride below. Each ride will be $2!</h3>
                <br>
                <?php 
                    $post = new Post($con, $userLoggedIn);
                    $post->loadPostMembers();
                ?>
            </form>
            

            
            <div class="posts_area"></div>
        </div>
    </div>
    </main>
</body>
<?php
    require "../view/footer.php";
?>

