-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2019 at 08:57 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uscbrides`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `avaliable_ride`
--

CREATE TABLE `avaliable_ride` (
  `idAvaliableRide` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(60) NOT NULL,
  `location` varchar(60) NOT NULL,
  `seats` varchar(2) NOT NULL,
  `user_closed` varchar(3) NOT NULL,
  `deleted` varchar(3) NOT NULL,
  `added_by` varchar(60) NOT NULL,
  `user_to` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `avaliable_ride`
--

INSERT INTO `avaliable_ride` (`idAvaliableRide`, `date`, `time`, `location`, `seats`, `user_closed`, `deleted`, `added_by`, `user_to`) VALUES
(6, '2019-04-30', '14:30', 'USCB Hilton Head Campus', '3', 'no', 'no', 'osano_saunders', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `car_info`
--

CREATE TABLE `car_info` (
  `idCar_Info` int(11) NOT NULL,
  `Make` varchar(45) DEFAULT NULL,
  `Model` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `year` int(4) NOT NULL,
  `licencePlate` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_info`
--

INSERT INTO `car_info` (`idCar_Info`, `Make`, `Model`, `color`, `year`, `licencePlate`) VALUES
(18, 'Hyundai', 'Elantra', 'Gold', 2018, 'Ht2073');

-- --------------------------------------------------------

--
-- Table structure for table `has`
--

CREATE TABLE `has` (
  `idHas` int(11) NOT NULL,
  `Car_Info_idCar_Info` int(11) NOT NULL,
  `Drivers_idDrivers` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `has`
--

INSERT INTO `has` (`idHas`, `Car_Info_idCar_Info`, `Drivers_idDrivers`) VALUES
(14, 18, 19);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `idMembers` int(11) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(235) DEFAULT NULL,
  `phoneNumber` varchar(15) DEFAULT NULL,
  `signup_date` date NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `user_closed` varchar(3) NOT NULL,
  `num_posts` int(11) NOT NULL,
  `ratings` int(11) NOT NULL,
  `has_car` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`idMembers`, `firstName`, `lastName`, `username`, `email`, `password`, `phoneNumber`, `signup_date`, `profile_pic`, `user_closed`, `num_posts`, `ratings`, `has_car`) VALUES
(19, 'Osano', 'Saunders', 'osano_saunders', 'Osano@email.uscb.edu', '5f4dcc3b5aa765d61d8327deb882cf99', '8432266023', '2019-04-16', '../images/profile_pics/defaults/head_sun_flower.png', 'no', 1, 0, 'yes'),
(20, 'Mickey', 'Mouse', 'mickey_mouse', 'Mickey@email.uscb.edu', '5f4dcc3b5aa765d61d8327deb882cf99', '8432266023', '2019-04-16', '../images/profile_pics/defaults/head_deep_blue.png', 'no', 0, 0, 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Indexes for table `avaliable_ride`
--
ALTER TABLE `avaliable_ride`
  ADD PRIMARY KEY (`idAvaliableRide`);

--
-- Indexes for table `car_info`
--
ALTER TABLE `car_info`
  ADD PRIMARY KEY (`idCar_Info`);

--
-- Indexes for table `has`
--
ALTER TABLE `has`
  ADD PRIMARY KEY (`idHas`),
  ADD KEY `fk_Has_Car_Info1_idx` (`Car_Info_idCar_Info`),
  ADD KEY `fk_Has_Drivers1_idx` (`Drivers_idDrivers`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`idMembers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `avaliable_ride`
--
ALTER TABLE `avaliable_ride`
  MODIFY `idAvaliableRide` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `car_info`
--
ALTER TABLE `car_info`
  MODIFY `idCar_Info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `has`
--
ALTER TABLE `has`
  MODIFY `idHas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `idMembers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `has`
--
ALTER TABLE `has`
  ADD CONSTRAINT `fk_Has_Car_Info1` FOREIGN KEY (`Car_Info_idCar_Info`) REFERENCES `car_info` (`idCar_Info`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Has_Drivers1` FOREIGN KEY (`Drivers_idDrivers`) REFERENCES `members` (`idMembers`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
