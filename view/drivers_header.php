<?php
require '../model/database.php';

if(isset($_SESSION["username"]))
{
    $userLoggedIn = $_SESSION["username"];
    $user_details_query = mysqli_query($con, "SELECT * FROM members WHERE username='$userLoggedIn'");
    $user = mysqli_fetch_array($user_details_query);
    
    
}
else{
    header("Location: ../login_page.php");
}

?>
<html>

<!-- the head section -->
<head>
    <meta charset="utf-8">
    <meta name="description" content="This is an example of a meta description. Will only show up in search results.">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <title>USCB RIDES</title>
    
    <!-- Javascript -->
    <script scr="http://ajax.googleapis.com/ajax/libs/jquary/2.1.3/jquary.min.js"></script>
    <script src="../js/bootstrap.js"></script>
    
    <!-- CSS -->
    <!-- image link icons are from fontawesome -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="../styles/main.css">
    <link rel="stylesheet" href="../styles/styles.css">

    
    
</head>

<body>
    <div class="top_bar">
            <div class="logo">
                <a href="index.php">USCB RIDES</a>
            </div>
        <nav>
            <a href="#">
                <?php echo $user['firstName'];?>
            </a>
            <a href="../drivers/driver_landing_page.php">
                <i class="fas fa-home"></i>
            </a>
            <a href="#">
                <i class="fas fa-cog"></i>
                
                <a href="../drivers/listings_page.php">
            <i class="fas fa-list"></i>
            </a>
            <a href="../includes/handlers/logout.php">
                <i class="fas fa-sign-out-alt"></i>
            </a>
        </nav>
        </div>
    
    <div class="wrapper">
        
