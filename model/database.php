<?php

// TESTING SQL DATABASE instead of SQLIte
//ob_start();// Turns on output buffer
session_start();

$timezone = date_default_timezone_set("America/New_York");
$dsn = 'mysql:host=localhost;dbname=uscbrides'; // hint: $dsn is not used by mysqli_connect
$username = 'root'; // in Sabalin LIBR241, 'root' -- on SABAL, 'osano'
$password = ''; // in LIBR241, 'sesame' -- on SABAL, ''
/*
try
{
    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $ex)
 {
     echo "Connection failed ". $ex->getMessage();
 }
 * 
 */
$con = mysqli_connect("localhost", $username, $password, "uscbrides");

if(mysqli_connect_errno())
{
    echo "failed to connect: ". mysqli_connect_errno();
}
